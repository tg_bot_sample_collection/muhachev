-- MySQL dump 10.13  Distrib 8.0.19, for macos10.15 (x86_64)
--
-- Host: localhost    Database: millioner
-- ------------------------------------------------------
-- Server version	8.0.23

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `answers`
--

DROP TABLE IF EXISTS `answers`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `answers` (
  `questionId` int NOT NULL,
  `text` varchar(50) NOT NULL,
  `right_answerr` tinyint(1) DEFAULT '0',
  KEY `questionId` (`questionId`),
  CONSTRAINT `answers_ibfk_1` FOREIGN KEY (`questionId`) REFERENCES `questions` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `answers`
--

LOCK TABLES `answers` WRITE;
/*!40000 ALTER TABLE `answers` DISABLE KEYS */;
INSERT INTO `answers` VALUES (1,'завтрак',0),(1,'обед',0),(1,'ужин',1),(1,'полдник',0),(2,'хлебу',0),(2,'кексу',0),(2,'бублику',0),(2,'прянику',1),(3,'пирожек',0),(3,'фуа-гра',1),(3,'кисель',0),(3,'яблоко',0),(4,'окрошку',0),(4,'гаспачо',0),(4,'рассольник',1),(4,'свекольник',0),(5,'онигири',0),(5,'кимчи',1),(5,'ушу',0),(5,'группу «BTS»',0),(6,'оливковое масло',1),(6,'томаты',0),(6,'моцарелла',0),(6,'базилик',0),(7,'грибы',0),(7,'петрушка',0),(7,'яблоки',1),(7,'клубника',0),(8,'бефстроганов',1),(8,'цыпленок табака',0),(8,'шницель по-венски',0),(8,'ростбиф',0),(9,'тэмпура',0),(9,'рамэн',0),(9,'суши',1),(9,'гедза',0),(10,'коктейли',0),(10,'фастфуд',0),(10,'шведский стол',1),(10,'еда на вынос',0),(11,'Джордж Байрон',0),(11,'Генрих Гейне',1),(11,'Поль Верлен',0),(11,'Гавриил Державин',0),(12,'ветвистой',1),(12,'сахаристой',0),(12,'кремнистой',0),(12,'мясистой',0),(13,'«Рославлев»',1),(13,'«Барышня-крестьянка»',0),(13,'«Метель»',0),(13,'«Путешествие в Арзрум»',0);
/*!40000 ALTER TABLE `answers` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2022-06-25 12:20:39

from database.connect import initDbConnection, closeConn


def getQuestion(config, number_question):
    context = initDbConnection(config)

    cursor = context.cursor()
    query = "SELECT * FROM questions WHERE section = " + str(number_question)
    cursor.execute(query)
    result = cursor.fetchone()
    closeConn(context)
    return result


async def getQuestionBySection(config, number_question):
    context = initDbConnection(config)

    cursor = context.cursor()
    query = "SELECT q.text, answers.* FROM answers " + \
            "JOIN questions q on q.id = answers.questionId " + \
            "WHERE section = " + str(number_question)
    cursor.execute(query)
    result = cursor.fetchall()
    closeConn(context)
    return result